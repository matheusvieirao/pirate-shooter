using UnityEngine;

public class ShipInputHandler : MonoBehaviour
{
    // Components
    ShipController shipController;
    private Cannon cannon;

    private void Awake()
    {
        shipController = GetComponentInChildren<ShipController>();
        cannon = GetComponentInChildren<Cannon>();
    }

    void Update()
    {
        Vector2 inputVector = Vector2.zero;

        inputVector.x = Input.GetAxis("Horizontal");
        inputVector.y = Input.GetAxis("Vertical");

        shipController.SetInputVector(inputVector);

        if (Input.GetButtonDown("Fire1"))
        {
            cannon.Shoot();
        }
    }
}
