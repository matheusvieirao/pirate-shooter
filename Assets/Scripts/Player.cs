using UnityEngine;

public class Player : Collideable
{
    private Health health;
    private EndGame endGame;

    private void Awake()
    {
        health = GetComponentInChildren<Health>();
        endGame = GameObject.FindGameObjectWithTag("LevelHandler").GetComponent<EndGame>();
    }

    private void Start()
    {
        health.SetInitialHealth(15);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            health.DoDamage(3);

        }
        if (health.IsOver())
        {
            endGame.GameOverScreen();
        }
    }

    protected override void OnCollision(GameObject goCollided)
    {
        if (goCollided.CompareTag("Chaser"))
        {
            health.DoDamage(3);
        }
        if (goCollided.CompareTag("EnemyCannonBall"))
        {
            health.DoDamage(1);
        }
    }

}
