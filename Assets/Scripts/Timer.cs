using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] float startTimeInSeconds = 90f;
    [SerializeField] TextMeshProUGUI textMeshProUGUI;
    private float currentTime;
    private bool isActive = false;
    private EndGame endGame;

    private void Awake()
    {
        endGame = GameObject.FindGameObjectWithTag("LevelHandler").GetComponent<EndGame>();
    }
    public void StartTimer()
    {
        isActive = true;
        currentTime = startTimeInSeconds;
    }
    void Update()
    {
        if (isActive)
        {
            currentTime = currentTime - Time.deltaTime;
            System.TimeSpan time = System.TimeSpan.FromSeconds(currentTime);
            textMeshProUGUI.text = time.ToString(@"m\:ss");
        }
        if (currentTime < 0)
        {
            isActive = false;
            currentTime = 0;
            endGame.GameWinScreen();
        }
    }
}
