using System;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    private int score = 0;
    [SerializeField] TextMeshProUGUI textMeshProUGUI;
    void Start()
    {
        UpdateScore();
    }

    private void UpdateScore()
    {
        textMeshProUGUI.text = score.ToString();
    }

    public void AddScore (int value)
    {
        score += value;
        UpdateScore();
    }

    internal int GetScore()
    {
        return score;
    }
}
