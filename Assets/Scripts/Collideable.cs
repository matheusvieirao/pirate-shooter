using UnityEngine;

public abstract class Collideable : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollision(collision.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        OnCollision(collider.gameObject);
    }
    abstract protected void OnCollision(GameObject goCollided);
}
