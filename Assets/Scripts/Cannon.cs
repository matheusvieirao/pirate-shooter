using UnityEngine;

public class Cannon : MonoBehaviour
{
    public Transform cannonTransform;
    public GameObject bulletPrefab;

    public float bulletForce = 0.01f;

    public void Shoot()
    {
        Vector3 position = cannonTransform.position + (cannonTransform.right.normalized * 0.5f);
        GameObject bullet = Instantiate(bulletPrefab, position, cannonTransform.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(cannonTransform.right * bulletForce, ForceMode2D.Impulse);
        Destroy(bullet, 2);
    }
}
