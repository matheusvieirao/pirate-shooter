using UnityEngine;

public class Health : MonoBehaviour
{
    private int health;
    private HealthBar healthBar = null;

    private void Awake()
    {
        healthBar = GetComponent<HealthBar>();
    }
    public Health (int health)
    {
        this.health = health;
        healthBar?.SetHealth(health);
    }

    public void DoDamage (int damage)
    {
        health -= damage;
        healthBar?.SetHealth(health);
    }

    public bool IsOver ()
    {
        return health <= 0;
    }

    public void SetInitialHealth(int health)
    {
        this.health = health;
        healthBar?.SetMaxHealth(health);

    }

    public int GetHealth()
    {
        return health;
    }
}
