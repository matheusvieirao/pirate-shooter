using UnityEngine;

public class Bullet : Collideable
{
    protected override void OnCollision(GameObject goCollided)
    {
        if (!goCollided.CompareTag("Land"))
        {
            Destroy(gameObject);
        }
    }
}
