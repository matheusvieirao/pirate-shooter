using Pathfinding;
using UnityEngine;

public class Chaser : Collideable
{
    [SerializeField]
    private Health health;
    private AIDestinationSetter aIDestinationSetter;
    private Transform playerTransform;
    private Score score;

    private void Awake()
    {
        health = GetComponentInChildren<Health>();
        aIDestinationSetter = GetComponent<AIDestinationSetter>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        score = GameObject.FindGameObjectWithTag("LevelHandler").GetComponent<Score>();
    }

    private void Start()
    {
        health.SetInitialHealth(2);
        aIDestinationSetter.target = playerTransform;
    }

    protected override void OnCollision(GameObject goCollided)
    {
        if (goCollided.CompareTag("Player"))
        {
            health.DoDamage(2);
            if (health.IsOver())
            {
                Destroy(gameObject);
            }
        }
        if (goCollided.CompareTag("PlayerCannonBall"))
        {
            health.DoDamage(1);
            if (health.IsOver())
            {
                score.AddScore(200);
                Destroy(gameObject);
            }
        }
    }
}
