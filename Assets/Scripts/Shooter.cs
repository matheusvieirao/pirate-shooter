using UnityEngine;

public class Shooter : Collideable
{
    private Transform player;
    private new Rigidbody2D rigidbody2D;
    private Cannon cannon;
    private Health health;
    private Score score;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        cannon = GetComponentInChildren<Cannon>();
        health = GetComponentInChildren<Health>();
        score = GameObject.FindGameObjectWithTag("LevelHandler").GetComponent<Score>();
    }

    private void Start()
    {
        InvokeRepeating("Shoot", 2.0f, 2.0f);
        health.SetInitialHealth(4);
    }

    void Update()
    {
        Vector3 direction = player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
        rigidbody2D.rotation = angle;
    }

    void Shoot()
    {
        cannon.Shoot();
    }

    protected override void OnCollision(GameObject goCollided)
    {
        if (goCollided.CompareTag("PlayerCannonBall"))
        {
            health.DoDamage(1);
            if (health.IsOver())
            {
                Destroy(gameObject);
                score.AddScore(300);
            }
        }
    }
}
