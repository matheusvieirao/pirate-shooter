using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField] Tilemap waterTilemap;
    [SerializeField] Tilemap landTilemap;
    [SerializeField] Tilemap wallTilemap;
    private Vector3Int waterMapSize;
    [SerializeField] GameObject shooterPrefab;
    [SerializeField] GameObject chaserPrefab;

    private int halfMapLenght;
    private int halfMapHeight;
    private ArrayList shooterLocations;
    // Start is called before the first frame update
    void Start()
    {
        waterTilemap.CompressBounds();   
        waterMapSize = waterTilemap.cellBounds.size;
        halfMapLenght = waterMapSize.x / 2;
        halfMapHeight = waterMapSize.y / 2;
        shooterLocations = new ArrayList();
        InvokeRepeating("SpawnShooter", 7f, 30.0f);
        InvokeRepeating("SpawnChaser", 0f, 15.0f);
    }


    private void SpawnShooter()
    {
        Vector3Int? location = SpawnNewEnemy(shooterPrefab);
        if (location != null)
            shooterLocations.Add(location);
    }


    private void SpawnChaser()
    {
        SpawnNewEnemy(chaserPrefab);
    }

    private Vector3Int? SpawnNewEnemy(GameObject enemyPrefab)
    {
        int randomX = 0, randomY = 0;
        bool foundPosition = false;
        int i = 0;
        while (!foundPosition)
        {
            i++;
            randomX = Random.Range(0, waterMapSize.x) - halfMapLenght;
            randomY = Random.Range(0, waterMapSize.y) - halfMapHeight;
            Vector3Int location = new Vector3Int(randomX, randomY, 0);
            if (IsAvaiableLocation(location))
            {
                foundPosition = true;
                Vector3 position = waterTilemap.GetCellCenterWorld(location);
                position.z = 8;
                GameObject shooter = Instantiate(enemyPrefab, position, enemyPrefab.transform.rotation);
                return location;
            }
            else if (i > 100)
            {
                foundPosition = true;
                Debug.Log("Position for Shooter enemy not found");
            }
        }
        return null;
    }

    private bool IsAvaiableLocation(Vector3Int location)
    {
        if (shooterLocations.Contains(location))
            return false;
        else
            return wallTilemap.GetTile(location) == null && landTilemap.GetTile(location) == null;
    }
}
