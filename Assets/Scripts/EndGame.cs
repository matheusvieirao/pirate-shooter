using TMPro;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    [SerializeField]
    private GameObject panelEndGame;
    private TextMeshProUGUI textMeshProUGUI;
    private Score score;

    private void Awake()
    {
        score = GameObject.FindGameObjectWithTag("LevelHandler").GetComponent<Score>();
        textMeshProUGUI = panelEndGame.GetComponentInChildren<TextMeshProUGUI>();
    }

    public void GameOverScreen()
    {
        ShowEndGameScreen("Game Over");
    }

    public void GameWinScreen()
    {
        ShowEndGameScreen("Weel done!");
    }

    private void ShowEndGameScreen(string message)
    {
        panelEndGame.SetActive(true);
        string scoreString = score.GetScore().ToString();
        string fullMessage = message + "\n\nScore: " + scoreString;
        textMeshProUGUI.text = fullMessage;
    }
}
