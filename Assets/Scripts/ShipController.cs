using UnityEngine;

public class ShipController : MonoBehaviour
{
    [Header("Ship settings")]
    public float accelerationFactor = 3f;
    public float turnFactor = 6f;
    public float orthogonalInertiaFactor = 0.5f;
    public float limitSteerWhenSlowFactor = 0.8f;
    public float fowardInertiaFactor = 0.5f;
    public float maxSpeed = 2.5f;

    // Local Variables
    float accelerationInput = 0;
    float steeringInput = 0;

    float rotationAngle = 0;

    float velocityUp = 0;

    // Components
    Rigidbody2D shipRigidBody2D;

    private void Awake()
    {
        shipRigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Frame-rate independent for physics calculations
    private void FixedUpdate()
    {
        ApplyEngineForce();
        ApplySteering();
        KillOrthogonalVelocity();
    }

    private void ApplyEngineForce()
    {
        // Calculate how much "forward" we are going in terms of the direction of our velocity
        velocityUp = Vector2.Dot(transform.up, shipRigidBody2D.velocity);

        // Limit so we cannot go faster than the certain speed fowrard and backwards
        if (velocityUp > maxSpeed && accelerationInput > 0)
            return;
        if (velocityUp < - maxSpeed * 0.2 && accelerationInput < 0)
            return;


        //Apply drag if there is no accelerationInput so the car stops when the player lets go of the accelerator
        if (accelerationInput == 0)
            shipRigidBody2D.drag = fowardInertiaFactor;
        else
            shipRigidBody2D.drag = 0;

        // Create a force for the engine
        Vector2 engineForceVector = transform.up * accelerationInput * accelerationFactor;

        // Apply force and pushes the ship forward
        shipRigidBody2D.AddForce(engineForceVector, ForceMode2D.Force);
    }

    // Kill part of orthogonal velocity in order to the ship go mainly forward, principally when it is dragging
    private void KillOrthogonalVelocity()
    {
        Vector2 fowardVelocity = transform.up * Vector2.Dot(shipRigidBody2D.velocity, transform.up);
        Vector2 rightVelocity = transform.right * Vector2.Dot(shipRigidBody2D.velocity, transform.right);

        shipRigidBody2D.velocity = fowardVelocity + rightVelocity * orthogonalInertiaFactor;
    }

    private void ApplySteering()
    {
        // Limit the ship hability to turn when moving slowly
        float minSpeedBeforeAllowTurningFactor = (shipRigidBody2D.velocity.magnitude / 8);
        minSpeedBeforeAllowTurningFactor = Mathf.Clamp01(minSpeedBeforeAllowTurningFactor) * limitSteerWhenSlowFactor;

        // Update the rotation angle based on input
        rotationAngle -= steeringInput * turnFactor * minSpeedBeforeAllowTurningFactor;

        // Apply steering by rotating the car object
        shipRigidBody2D.MoveRotation(rotationAngle);
    }

    public void SetInputVector(Vector2 inputVector)
    {
        steeringInput = inputVector.x;
        accelerationInput = inputVector.y;
    }
}
